# Firebase Hosting, React JS and Gitlab

In this .gitlab-ci.yml there is a example code of how use DevOps GitLab to public a project in React use Firebase Hosting.

Next I will list a series of resources that you need to know to fully understand the code:
- [Environment Variables](https://docs.gitlab.com/ee/ci/variables/)
- [Public React JS in Firebase](https://www.robinwieruch.de/firebase-deploy-react-js)
